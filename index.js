const db = require("./db.js");
var express = require("express");
var jsonInf = "";
var http = require("http");
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }));

// configuración + rutas
app.get("/", function(req, res){
    db.query("SELECT * from app;", [], onData);
    function onData(err, data){
        if(!err){
           console.log(data.rows);

            res.end([
                "<script>",
                    "function recuperarText(){",
                        "var porId=document.getElementById('recuperar').value;",
                        "if(porId == ''){",
                            "porId = 'error'",
                        "}",
                        "else{",
                            "location.href =window.location + porId;",
                        "}",
                    "}",
                    "function afegirRecord(){",
                        "var porId=document.getElementById('appCode').value;",
                        "if(porId == ''){",
                            "porId = 'error'",
                        "}",
                        "document.getElementById('form').setAttribute('action', '' + porId);",
                    "}",
                "</script>",
                "<h1>List of apps </h1>",
                "<p>List of appGames: </p>",
                "<p>" + jsonInf + "</p>",
                "<input type='text' name='recuperar' id='recuperar'>",
                "<button onclick='recuperarText()'>",
                    "Recover record",
                "</button>",
                "<form method='post' action = '/error' id='form'/>",
                    "<p>App code:</p>",
                    "<input type='text' name='appCode' id='appCode'>",
                    "<p>Name:</p>",
                    "<input type='text' name='name' id='name'>",
                    "<p>Scores:</p>",
                    "<input type='text' name='score' id='score'>",
                    "<button onclick='afegirRecord()'>",
                        "New Record",
                    "</button>",
                    "<form/>"
            ].join('\n'));
        }
        else{
            console.log("Error: ", err);
        }
    }
})

app.get("/:app_code", function(req, res){
    var id = req.params.app_code;
    db.query("SELECT count(*) from app WHERE app_code = $1", [id], exist);
        
    function exist(err, data){
        if(!err){
            if( data.rows[0].count == 0){
                res.sendStatus(404);
                res.end([
                    "Error 404. This application does not exist"
                ].join('\n'));
            }
            else{
                db.query("SELECT player, score, datetime from record WHERE app_code = $1;", [id], onData);
                function onData(err, data){
                    if(!err){
                        if(data.rowCount == 0){
                            res.end([
                                "<h1>Records of this app</h1>",
                                "<p>There aren't records of this app</p>",
                            ].join('\n'));
                        }
                        else{
                            jsonInf = JSON.stringify(data.rows);
                            console.log(data.rows);
                            res.end(jsonInf);
                        }
                        
                    }
                    else{
                        console.log("Error: ", err);
                    }
                }
            }
        }
        else{
            console.log("Error: ", err);
        }
    }
})

app.post("/:app_code", function(req, res){
    if (req.body.appCode == ""){
        res.status(500);
        res.end(["Error 500"].join('\n'));
    }
    else if(req.body.name == "" || req.body.score == ""){
        res.status(422);
        res.end(["Error 422. It has not been indicated the name of the player or the scores"].join('\n'))
    }
    
    else{
        var id = req.params.app_code;
        db.query("INSERT INTO record (app_code, player, score) VALUES ($1, $2, $3);", [id, req.body.name, req.body.score], onData);
        function onData(err, data){
            if(!err){
               res.end(["<h1>New record: </h1>",
                        "[<p>Player: " + req.body.name + "</p>",
                        "<p> Score: " +  req.body.score + "</p>]"].join('\n'));
            }
            else{
                res.status(500);
                res.end(["Error 500"].join('\n'));
            }
        }
    }
})

http.createServer(app).listen(process.env.PORT || 3000);
